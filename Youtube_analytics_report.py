from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client.tools import argparser
import re
import dateutil.parser

DEVELOPER_KEY = "AIzaSyCCCX01Xl1v2zzhZquYM5UAIu6mKU-ekAo"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"
VIDEOS_AFTER = "2015-08-07T00:00:01.000Z"
FILE_FORMAT = "csv"
SEPARATOR = ";"  # comma appears in many video titles

# CHANNEL_IDS = ["UC14UlmYlSNiQCBe9Eookf_A", "UC5Z9MJweg-S4KSmWdBH6cnw"]  # example channelIds
CHANNEL_IDS = []  # example channelIds
USER_NAMES = []


def youtube_search(options, next_page_token, chan_id, user_name):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)
    condition = 0
    videos = []
    if chan_id is None:
        search_channel_for_id = youtube.channels().list(
            part="id",
            forUsername=user_name
        ).execute()
        chan_id = search_channel_for_id.get("items")[0]["id"]

    while condition == 0:
        # Call the search.list method to retrieve results matching the specified query term.
        if next_page_token is not None:
            search_response = youtube.search().list(
                channelId=chan_id,
                part="id,snippet",
                order="date",
                pageToken=next_page_token,
                maxResults=50
            ).execute()
        else:
            search_response = youtube.search().list(
                channelId=chan_id,
                part="id,snippet",
                order="date",
                maxResults=50
            ).execute()

        search_videos = []

        # Merge video ids
        for search_result in search_response.get("items", []):
            try:
                search_videos.append(search_result["id"]["videoId"])
                video_ids = ",".join(search_videos)
            except Exception:
                condition = 1

        # Call the videos.list method to retrieve location details for each video.

        try:
            video_ids
        except NameError:
            print('No videos in channelId: %s', chan_id)
            break
        else:
            video_response = youtube.videos().list(
                id=video_ids,
                part='snippet, contentDetails'
            ).execute()

            # Add each result to the list, and then display the list of matching videos.
            for video_result in video_response.get("items", []):
                videos.append("%s %s %s %s" % (video_result["snippet"]["channelTitle"] + SEPARATOR,
                                               video_result["snippet"]["title"] + SEPARATOR,
                                               video_result["snippet"]["publishedAt"] + SEPARATOR,
                                               parse_ISO8601(video_result["contentDetails"]["duration"])))

            if dateutil.parser.parse(VIDEOS_AFTER) < \
                    dateutil.parser.parse(
                        video_response.get("items")[len(video_response) - 1]["snippet"]["publishedAt"]):
                next_page_token = search_response.get("nextPageToken")
            else:
                condition = 1

    try:
        # for Barcelona cannot save the files, for instance ñ
        save_to_file(video_result, videos)
    except Exception:
        print('Cannot save the file')

    print("Videos:\n", "\n".join(videos), "\n")


def parse_ISO8601(dur):
    test1 = re.search("PT(.+?)H", dur)
    if test1:
        return int(test1.group(1)) * 3600
    test2 = re.search("PT(.+?)H(.+?)M", dur)
    if test2:
        h, m = [int(x) for x in test2.groups()]
        return 3600 * h + 60 * m
    test3 = re.search("PT(.+?)H(.+?)S", dur)
    if test3:
        h, s = [int(x) for x in test3.groups()]
        return 3600 * h + s
    test4 = re.search("PT(.+?)H(.+?)M(.+?)S", dur)
    if test4:
        h, m, s = [int(x) for x in test4.groups()]
        return 3600 * h + 60 * h + s
    test5 = re.search("PT(.+?)M", dur)
    if test5:
        return int(test5.group(1)) * 60
    test6 = re.search("PT(.+?)M(.+?)S", dur)
    if test6:
        m, s = [int(x) for x in test6.groups()]
        return 60 * m + s
    test7 = re.search("PT(.+?)S", dur)
    if test7:
        return int(test7.group(1))
    print("CAN'T PARSE GOOGLE FORMATTING:", dur)
    return 0  # return 0 if cannot handle


def save_to_file(video_result, videos):
    f = open(video_result["snippet"]["channelTitle"] + "." + FILE_FORMAT, 'w')
    for item in videos:
        f.write("%s \n" % item)  # encode()
    f.close()


def read_from_file(file_name):
    f = open(file_name, 'r+')
    links = f.readlines()
    f.close()
    return links


def extract_id_from_link(text):
    return text.split('channel/', 1)[-1].split('user/', 1)[-1].split('/', 1)[0]


if __name__ == "__main__":
    argparser.add_argument("--channel", help="Links with channelId")
    argparser.add_argument("--user", help="Links with user")
    args = argparser.parse_args()

    if args.user is not None:
        USER_NAMES = read_from_file(args.user)
        user_names_copy = []
        for i in USER_NAMES:
            user_names_copy.append(extract_id_from_link(i))
        USER_NAMES = user_names_copy

    if args.channel is not None:
        CHANNEL_IDS = read_from_file(args.channel)
        channel_id_copy = []
        for i in CHANNEL_IDS:
            channel_id_copy.append(extract_id_from_link(i))
        CHANNEL_IDS = channel_id_copy

    try:
        for channel_id in CHANNEL_IDS:
            page_token = None
            youtube_search(args, page_token, channel_id, None)
        for user_name in USER_NAMES:
            page_token = None
            youtube_search(args, page_token, None, user_name)
    except HttpError as e:
        print("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))
