from setuptools import setup

setup(name='Youtune',
      version='0.1',
      description='The funniest joke in the world',
      url='http://gitlab.com/szymonk92/you',
      author='Szymon Klepacz',
      author_email='@example.com',
      license='MIT',
      packages=['YoutubeAnalysis'],
      install_requires=[
          'httplib2',
          'apiclient',
          'oauth2client'
      ],
      zip_safe=False)